(ns mastermind.core
  (:gen-class))



 (declare code-secret)
(defn code-secret
  "composition d'un code secret aléatoire"
  [c]
  (loop [i 0 r [] v [:rouge :bleu :vert :jaune :noir :blanc]]
    (if (< i 4)
      (recur (inc i) (conj r (get v (rand-int c))) v)
      r)))

(declare freqs-dispo)
(defn freqs-dispo
  [map repons]
  (loop [map map , repons repons, res {}]
    (if (seq map)
      (if (= (first repons) :good)
        (if (contains? res (first map))
          (recur (rest map) (rest repons) res)
          (recur (rest map) (rest repons) (assoc res (first map) 0)))
        (if (contains? res (first map))
          (recur (rest map) (rest repons) (assoc res (first map) (inc (get res (first map)))))
          (recur (rest map) (rest repons) (assoc res (first map) 1)))
        )
      res
      )
    ))

(declare filtre-indications)
(defn filtre-indications
  "verifie si l'indication est bonne"
  [map repons indic]
  (let [dispofre (freqs-dispo map indic)]
    (loop [dispofre dispofre, indic indic, i 0]
      (if (< i 4)
        (if (= (nth indic i) :good)
          (recur dispofre indic (inc i))
          (if (= (get dispofre (get repons i)) nil)
            (recur dispofre indic (inc i))
            (if (zero? (get dispofre (get repons i)))
              (recur dispofre  (assoc indic i :bad) (inc i))
              (recur (assoc dispofre (get repons i) (dec (get dispofre (get repons i)))) indic (inc i)))))
        indic))))

(declare mode)
(defn mode [a]
  (if (= a 1)
    true
    false))
(declare containe)
(defn containe
  [c v]
  (loop [i 0]
    (if (< i 4)
      (if (= (get c i) v)
        true
        (recur (inc i)))
      false)))
 
(declare indications)
(defn indications
  "indication sur la reponse proposer"
  [a b]
  (loop [i 0 r []]
    (if (< i 4)
      (if (containe a (get b i))
        (if (= (get a i) (get b i))
          (recur (inc i) (conj r :good))
          (recur (inc i) (conj r :color)))
        (recur (inc i) (conj r :bad)))
      r)))

(defn myread []
  (loop [i 0,res []]
    (if (< i 4)
      (recur (inc i) (conj res (read)))
      res)))

;;"choisir un mode (1: 1P vs COM, 2: COM vs 2p)"
(declare index)
(defn index []
  (println "Mastermind")
  (let [nmode (read)]
    (if (mode nmode)
      (println "1P vs COM")
      (println " COM vs 2p")))

  (println "Saisir votre reponse")
  (let [maps (code-secret 4) repons (read)]
    (println (indications maps repons))))


(declare affichePion)
(defn affichePion [indic]
  (let [pion {:good 0 :color 0}]
    (loop [i 0, indic indic, res pion]
      (if (< i 4)
        (if (or (= :good (nth indic i)) (= :color (nth indic i)))
          (recur (inc i) indic (assoc res (nth indic i) (inc (get res (nth indic i)))))
          (recur (inc i) indic res))
        res))
    )
  )

(defn mode1 []
  (println "1P vs COM")
  (println "Saisir votre reponse")
  (println "Exemple: :vert :rouge :bleu :jaune")

  (let [maps (code-secret 6) ,repons (myread)]
    (loop [i 0, repons repons]
      (flush)
      (if (< i 4)
        (do 
          (if (= [:good :good :good :good] (indications maps repons))
            (println "YOU WIN  good : 4  color : 0")
            (do  (println (filtre-indications maps repons (indications maps repons)))
                 (println (affichePion  (filtre-indications maps repons (indications maps repons))))
                 (recur (inc i) (myread)))))
        (do  (println "Corrections")
             (println maps)
             (println "YOU LOSE")))
      )))

(defn prendreColor [listbad2]
  ;;prendre un color qui n'est pas dans la listbad
  ;;Question : :vert :vert :vert :vert == good color bad bad
  (let [color [:rouge :bleu :vert :jaune :noir :blanc]]
    (loop [Uncolor (get color (rand-int 6))]
      (if (containe listbad2 Uncolor)
        (recur (get color (rand-int 6)))
        Uncolor))))

(defn changerP [listbad3 colorV]
  ;;changer la position. changer la color a la fin
  (let [listbad4 (conj listbad3 colorV)]
     (prendreColor listbad4))
 )


(defn ger2 [repons  indicU listbad]
  ;;gerener lecode
  (loop [j 0,repons2 repons ,listbad listbad]
    (if (< j 4)
      (do
        ;;(println repons2)
       ;;(println listbad)
        (case (nth indicU j)
          :good (recur (inc j) repons2 listbad)
          :bad (do
                 (recur (inc j) (assoc repons2 j (prendreColor (conj listbad (nth repons j)))) (conj listbad (nth repons j))))
          (do
            (assoc repons j (changerP listbad (nth repons j)))
            (recur (inc j) (assoc repons2 j (changerP listbad (nth repons2 j))) listbad))))
      repons2)))

(defn addlistbad [repons indicU]
  (loop [j 0,repons2 repons ,indicU indicU,listbad []]
    (if (< j 4)
      (do
        (case (nth indicU j)
          :bad (recur (inc j) repons2 indicU (conj listbad (nth repons j)))
          (recur (inc j) repons2 indicU listbad)))
      listbad)))

;;(def listbad [])
(defn mode2 [nb]
  (println "COM vs 2p")
  (println "Saisir votre Map")
  (println "Exemple: :vert :rouge :bleu :jaune")


  (let [maps (myread) ,repons [:rouge :jaune :vert :bleu], listbad []]
    (loop [i 0, indic [], listbad2 listbad, repons repons ]
      (if (< i nb)
        (do
          (println repons)
          ;;(println listbad)
          (let [indicU  (myread)]
            (if (= indicU [:good :good :good :good])
              (println "COM Win")
              (do
                ;;(genererCode repons indicU listbad)                
                (recur (inc i)  indicU (addlistbad repons indicU) (ger2  repons indicU listbad2) )))))
        (println "COM LOSE")))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  
  (println "Hello!")
  (println "Mastermind")
  (println "Saisir le numero du mode")
  (println "1: 1P vs COM ")
  (println "2: COM vs 1P")
  (let [nmode (read)]
    (if (mode nmode)
      (mode1)
      (mode2 5)))

  )
