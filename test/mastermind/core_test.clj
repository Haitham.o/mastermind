(ns mastermind.core-test
  (:require [clojure.test :refer :all]
            [mastermind.core :refer :all]
            [midje.sweet :refer :all]))

(deftest a-test
  (testing "FIXME, I fail."
    (is (= 0 1))))


(fact "Le `code-secret` est bien composé de couleurs."
      (every? #{:rouge :bleu :vert :jaune :noir :blanc}
              (code-secret 6))
      => true)
      
(fact "Les fréquences disponibles de `freqs-dispo` sont correctes."
      (get {:bleu 1, :rouge 2, :vert 0} (first [:rouge :rouge :bleu :vert :rouge]))
      (freqs-dispo [:rouge :rouge :bleu :vert :rouge]
                   [:good :color :bad :good :color])
      => {:bleu 1, :rouge 2, :vert 0})

(fact "Le `filtre-indications` fonctionne bien."
      (filtre-indications [:rouge :rouge :vert :bleu]
                          [:vert :rouge :bleu :jaune]
                          [:color :good :color :bad])
      => [:color :good :color :bad]

      (filtre-indications [:rouge :vert :rouge :bleu]
                          [:rouge :rouge :bleu :rouge]
                          [:good :color :color :color])
      => [:good :color :color :bad])

(fact "Le `mode` fonctionne bien."
      (mode 1) => true
      (mode 2) => false)

(fact "`containe` fonctionne correctement."

      (containe  [:rouge :rouge :vert :bleu]  :vert) => true
      (containe  [:rouge :rouge :vert :bleu]  :bleu) => true)


(fact "`indications` sont les bonnes." 
      (indications [:rouge :rouge :vert :bleu]
                   [:vert :rouge :bleu :jaune])
      => [:color :good :color :bad]

      (indications [:rouge :rouge :vert :bleu]
                   [:bleu :rouge :vert :jaune])
      => [:color :good :good :bad]

      (indications [:rouge :rouge :vert :bleu]
                   [:rouge :rouge :vert :bleu])
      => [:good :good :good :good]

      (indications [:rouge :rouge :vert :vert]
                   [:vert :bleu :rouge :jaune])
      => [:color :bad :color :bad])


(fact "Le `myread` fonctionne bien."
      (myread))


(fact "`affichePion` renvoi le bon vecteur."
      (affichePion [:good :bad :color :good]) 
      => {:good 2, :color 1})

(fact "`prendreColor` renvoi une couleur qui n'est pas 
contenu dans la list passer en parametre"
      (containe [:jaune :blanc :noir] 
                (prendreColor [:rouge :bleu :vert]))
      (= false (containe [:rouge :vert :bleu]
                         (prendreColor [:rouge :bleu :vert]))))

(fact "`changerP` permet de choisir une autre couleur qui n'est ni 
contenu dans la liste bad ni celle qui etait color juste avant"
      (containe [:jaune :blanc]
                (changerP [:rouge :bleu :vert] :noir))
      (= false (containe [:rouge :vert :bleu :noir]
                         (changerP [:rouge :bleu :vert] :noir))))